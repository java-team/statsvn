Source: statsvn
Section: vcs
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: ant,
               debhelper-compat (= 13),
               default-jdk,
               javahelper (>= 0.36),
               libjcommon-java,
               libjdom1-java,
               libjfreechart-java,
               libsvnkit-java,
               statcvs (>= 1:0.5.0),
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/java-team/statsvn.git
Vcs-Browser: https://salsa.debian.org/java-team/statsvn
Homepage: http://www.statsvn.org

Package: statsvn
Architecture: all
Depends: default-jre | java-runtime,
         java-wrappers (>= 0.1.22),
         statcvs (>= 1:0.7.0.dfsg-4~),
         subversion,
         ${java:Depends},
         ${misc:Depends},
Description: SVN repository statistics
 StatSVN retrieves information from a Subversion repository and generates
 various tables and charts describing the project evolution, e.g.
 the lines of code over time, contribution of each developer, the
 evolution of modules, directories, files, the time and days when most
 checkins happen, etc. It also shows the commit logs and integrates
 out of the box with ViewVc, BugZilla, Chora and others.
 .
 StatSVN generates a static suite of HTML or XDoc documents containing
 tables and chart images.
